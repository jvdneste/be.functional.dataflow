package be.functional.util.functional.tuple;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.IntSupplier;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ComparisonChain;

import be.functional.util.functional.IntSuppliers;

/**
 * Represents a tuple of 4 elements
 */
public class Quad<A,B,C,D> {

	public final A first;
	public final B second;
	public final C third;
	public final D fourth;

	private final IntSupplier hashcode;

	/** use Quad.of instead */
	protected Quad(final A a, final B b, final C c, final D d) {
		this.first = a;
		this.second = b;
		this.third = c;
		this.fourth = d;
		this.hashcode = IntSuppliers.memoize(() -> Objects.hash(a, b, c, d));
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("first", first).add("second", second).add("third", third).add("fourth", fourth).toString();
	}

	@Override
	public int hashCode() {
		assert hashcode.getAsInt() == Objects.hash(first, second, third, fourth);
		return hashcode.getAsInt();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Quad<?,?,?,?> other = (Quad<?,?,?,?>) obj;
		if (hashCode() != other.hashCode()) {
			return false;
		}
		return Objects.equals(first, other.first)
				&& Objects.equals(second, other.second)
				&& Objects.equals(third, other.third)
				&& Objects.equals(fourth, other.fourth);
	}

	public static <A,B,C,D> Quad<A,B,C,D> of(final A a, final B b, final C c, final D d) {
		return new Quad<A, B, C, D>(a, b, c, d);
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C,D> Function<Quad<A,B,C,D>,A> extractFirst() {
		return ExtractFirst;
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C,D> Function<Quad<A,B,C,D>,B> extractSecond() {
		return ExtractSecond;
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C,D> Function<Quad<A,B,C,D>,C> extractThird() {
		return ExtractThird;
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C,D> Function<Quad<A,B,C,D>,D> extractFourth() {
		return ExtractFourth;
	}

	@SuppressWarnings("rawtypes")
	private static final Function ExtractFirst = from -> ((Quad)from).first;

	@SuppressWarnings("rawtypes")
	private static final Function ExtractSecond = from -> ((Quad)from).second;

	@SuppressWarnings("rawtypes")
	private static final Function ExtractThird = from -> ((Quad)from).third;

	@SuppressWarnings("rawtypes")
	private static final Function ExtractFourth = from -> ((Quad)from).fourth;


	@SuppressWarnings("rawtypes")
	private static final Comparator RawComparator = (o1, o2) -> {
		final Quad t1 = (Quad) o1, t2 = (Quad) o2;
		return ComparisonChain.start()
				.compare((Comparable) t1.first, (Comparable) t2.first)
				.compare((Comparable) t1.second, (Comparable) t2.second)
				.compare((Comparable) t1.third, (Comparable) t2.third)
				.compare((Comparable) t1.fourth, (Comparable) t2.fourth)
				.result();
	};

	/**
	 * A lexicographical Quad Comparator
	 */
	@SuppressWarnings("unchecked")
	public static <A extends Comparable<? super A>, B extends Comparable<? super B>, C extends Comparable<? super C>, D extends Comparable<? super D>>
	Comparator<Quad<? extends A, ? extends B, ? extends C, ? extends D>> comparator() {
		return RawComparator;
	}
}
