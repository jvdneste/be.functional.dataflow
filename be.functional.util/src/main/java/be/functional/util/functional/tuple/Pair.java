package be.functional.util.functional.tuple;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.IntSupplier;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ComparisonChain;

import be.functional.util.functional.IntSuppliers;

/**
 * Represents a tuple of 2 elements
 */
public class Pair<A, B> {

	public final A first;
	public final B second;

	private final IntSupplier hashcode;

	/** use Pair.of instead */
	protected Pair(final A a, final B b) {
		this.first = a;
		this.second = b;
		this.hashcode = IntSuppliers.memoize(() -> Objects.hash(a, b));
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("first", first).add("second", second).toString();
	}

	@Override
	public int hashCode() {
		assert hashcode.getAsInt() == Objects.hash(first, second);
		return hashcode.getAsInt();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Pair<?,?> other = (Pair<?,?>) obj;
		if (hashCode() != other.hashCode()) {
			return false;
		}
		return Objects.equals(first, other.first) && Objects.equals(second, other.second);
	}

	public static <A,B> Pair<A, B> of(final A a, final B b) {
		return new Pair<A,B>(a, b);
	}

	@SuppressWarnings("rawtypes")
	private static final Function ExtractFirst = from -> {
		Preconditions.checkNotNull(from);
		return ((Pair)from).first;
	};

	@SuppressWarnings("unchecked")
	public static <A> Function<Pair<A,?>,A> extractFirst() {
		return ExtractFirst;
	}

	@SuppressWarnings("rawtypes")
	private static final Function ExtractSecond = from -> {
		Preconditions.checkNotNull(from);
		return ((Pair)from).second;
	};

	@SuppressWarnings("unchecked")
	public static <B> Function<Pair<?,B>,B> extractSecond() {
		return ExtractSecond;
	}

	@SuppressWarnings("rawtypes")
	private static final Comparator RawComparator = (o1, o2) -> {
		final Pair p1 = (Pair) o1, p2 = (Pair) o2;
		return ComparisonChain.start().compare((Comparable) p1.first, (Comparable) p2.first).compare((Comparable) p1.second, (Comparable) p2.second).result();
	};

	/**
	 * A lexicographical Pair Comparator
	 */
	@SuppressWarnings("unchecked")
	public static <A extends Comparable<? super A>, B extends Comparable<? super B>> Comparator<Pair<? extends A, ? extends B>> comparator() {
		return RawComparator;
	}
}
