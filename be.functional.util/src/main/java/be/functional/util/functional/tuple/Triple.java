package be.functional.util.functional.tuple;

import java.util.Comparator;
import java.util.Objects;
import java.util.function.IntSupplier;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ComparisonChain;

import be.functional.util.functional.IntSuppliers;

/**
 * Represents a tuple of 3 elements
 */
public class Triple<A,B,C> {

	public final A first;
	public final B second;
	public final C third;

	private final IntSupplier hashcode;

	/** use Triple.of instead */
	protected Triple(final A a, final B b, final C c) {
		this.first = a;
		this.second = b;
		this.third = c;
		this.hashcode = IntSuppliers.memoize(() -> Objects.hash(a, b, c));
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("first", first).add("second", second).add("third", third).toString();
	}

	@Override
	public int hashCode() {
		assert hashcode.getAsInt() == Objects.hash(first, second, third);
		return hashcode.getAsInt();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Triple<?,?,?> other = (Triple<?,?,?>) obj;
		if (hashCode() != other.hashCode()) {
			return false;
		}
		return Objects.equals(first, other.first)
				&& Objects.equals(second, other.second)
				&& Objects.equals(third, other.third);
	}

	public static <A,B,C> Triple<A,B,C> of(final A a, final B b, final C c) {
		return new Triple<A, B, C>(a, b, c);
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C> Function<Triple<A,B,C>,A> extractFirst() {
		return ExtractFirst;
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C> Function<Triple<A,B,C>,B> extractSecond() {
		return ExtractSecond;
	}

	@SuppressWarnings("unchecked")
	public static <A,B,C> Function<Triple<A,B,C>,C> extractThird() {
		return ExtractThird;
	}

	@SuppressWarnings("rawtypes")
	private static final Function ExtractFirst = from -> ((Triple)from).first;

	@SuppressWarnings("rawtypes")
	private static final Function ExtractSecond = from -> ((Triple)from).second;

	@SuppressWarnings("rawtypes")
	private static final Function ExtractThird = from -> ((Triple)from).third;

	@SuppressWarnings("rawtypes")
	private static final Comparator RawComparator = (o1, o2) -> {
		final Triple t1 = (Triple) o1, t2 = (Triple) o2;
		return ComparisonChain.start()
				.compare((Comparable) t1.first, (Comparable) t2.first)
				.compare((Comparable) t1.second, (Comparable) t2.second)
				.compare((Comparable) t1.third, (Comparable) t2.third)
				.result();
	};

	/**
	 * A lexicographical Triple Comparator
	 */
	@SuppressWarnings("unchecked")
	public static <A extends Comparable<? super A>, B extends Comparable<? super B>, C extends Comparable<? super C>>
	Comparator<Triple<? extends A, ? extends B, ? extends C>> comparator() {
		return RawComparator;
	}
}
