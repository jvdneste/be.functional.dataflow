package be.functional.util.functional;

import java.util.function.IntSupplier;

import com.google.common.base.Preconditions;

public class IntSuppliers {

	public static IntSupplier memoize(final IntSupplier src) {
		Preconditions.checkNotNull(src);
		return (src instanceof MemoizingIntSupplier)? src : new MemoizingIntSupplier(src);
	}

	private static class MemoizingIntSupplier implements IntSupplier {

		private IntSupplier source;
		private volatile boolean initialized;
		private int value;

		public MemoizingIntSupplier(final IntSupplier src) {
			source = src;
		}

		@Override
		public int getAsInt() {
			// A 2-field variant of Double Checked Locking.
			if (!initialized) {
				synchronized (this) {
					if (!initialized) {
						final int t = source.getAsInt();
						source = null;
						value = t;
						initialized = true;
						return t;
					}
				}
			}
			return value;
		}
	}
}
