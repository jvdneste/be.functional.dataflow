package be.functional.dataflow.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.Iterators;
import com.google.common.collect.UnmodifiableIterator;

import be.functional.dataflow.core.Expression;
import be.functional.dataflow.core.IDependent;
import be.functional.dataflow.core.IProperty;
import be.functional.dataflow.core.StmDomain;
import be.functional.dataflow.core.Value;
import be.functional.util.event.EventImpl;
import be.functional.util.event.IEvent;
import be.functional.util.functional.tuple.Pair;

// Currently ignoring ModelType or ModelDescription.

public class Model {

	private final Supplier<Map<String, Value<?>>> properties = Suppliers.memoize(() -> new HashMap<>());

	private final StmDomain domain;

	public Model(final StmDomain domain) {
		this.domain = domain;
	}

	protected void put(final String pKey, final Value<?> pDependable) {
		final Map<String, Value<?>> props = properties.get();
		if (cells.put(pKey, pDependable) == null) {
			_eventAdded.fire(Pair.<String,Value<?>>of(pKey, pDependable));
		}
	}

	protected <T> Expression<IProperty<T>> path(final String[] pPropertyNames) {
		Preconditions.checkArgument(pPropertyNames.length > 0, "Expected at least one property name.");
		class Path implements Function<IDependent,IProperty<T>> {
			@Override
			public IProperty<T> apply(final IDependent depender) {
				final UnmodifiableIterator<String> it = Iterators.forArray(pPropertyNames);

				Model model = Model.this;
				for (;;) {
					final String next = it.next();
					if (!it.hasNext()) {
						return model.getLocalProperty(next);
					}
					final IProperty<Model> property = model.<Model>getLocalProperty(next);
					model = property.get(depender);
					if (model == null) {
						property.set(new Model(domain));
						model = property.get(depender); // the property must know that we depend on it
					}
				}
			}
			@Override
			public String toString() {
				return MoreObjects.toStringHelper("Path").addValue(Arrays.toString(pPropertyNames)).toString();
			}
		};
		return domain.newExpression(new Path());
	}

	//	public <T> IProperty<T> getProperty(final String... pPropertyNames) {
	//		Preconditions.checkArgument(pPropertyNames.length > 0, "Expected at least one property name.");
	//		if (pPropertyNames.length == 1) {
	//			return getLocalProperty(pPropertyNames[0]);
	//		}
	//
	//		final Expression<IProperty<T>> path = path(pPropertyNames);
	//
	//		class PathProperty implements IProperty<T>, Expression<T> {
	//
	//			@Override
	//			public void set(final T pValue) {
	//				path.output().set(pValue);
	//			}
	//
	//			@Override
	//			public T get(final IDependent pDependant) {
	//				return path.get(pDependant).get(pDependant);
	//			}
	//
	//			@Override
	//			public String toString() {
	//				return MoreObjects.toStringHelper("PathProperty").add("path", Arrays.toString(pPropertyNames)).add("expression", path).toString();
	//			}
	//
	//			@Override
	//			public StmDomain getDomain() {
	//				return domain;
	//			}
	//
	//			@Override
	//			public T output() {
	//				return path.output().output();
	//			}
	//		}
	//
	//		return new PathProperty();
	//	}

	private <T> IProperty<T> getLocalProperty(final String pPropertyName) {
		final Map<String, Value<?>> props = properties.get();

		@SuppressWarnings("unchecked")
		IProperty<T> property = (IProperty<T>) props.get(pPropertyName);
		if (property == null) {
			property = domain.newProperty(null);
			props.put(pPropertyName, property);
		}

		return property;
	}

	//	@Override
	//	public Iterator<Pair<String, Value<?>>> iterator() {
	//		return Iterators.transform(properties.get().entrySet().iterator(), pEntry -> Pair.<String,Value<?>>of(pEntry.getKey(), pEntry.getValue()));
	//	}

	private final EventImpl<Pair<String, Value<?>>> _eventAdded = new EventImpl<>();

	public IEvent<Pair<String, Value<?>>> eventAdded() {
		return _eventAdded;
	}

	private final EventImpl<Pair<String, Value<?>>> _eventRemoved = new EventImpl<>();

	public IEvent<Pair<String, Value<?>>> eventRemoved() {
		return _eventRemoved;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper("Model").addValue(properties.get()).toString();
	}

	//  public void sideEffect(IDependable<?> ) {
	//
	//  }
	//
	//  public void applySideEffects() {
	//
	//  }
}
