package be.functional.dataflow.core;

public interface Expression<T> extends Value<T>, IDependent {

}
