package be.functional.dataflow.core;

public interface IDependent {

	Domain getDomain();

}
