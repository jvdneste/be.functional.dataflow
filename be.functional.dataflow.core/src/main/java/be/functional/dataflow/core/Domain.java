package be.functional.dataflow.core;

import java.lang.ref.WeakReference;

public interface Domain {

	String getName();

	void signal(Iterable<WeakReference<IDependent>> expressions);

}
