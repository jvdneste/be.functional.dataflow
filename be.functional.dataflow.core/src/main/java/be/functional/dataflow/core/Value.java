package be.functional.dataflow.core;

public interface Value<T> {

	T get(IDependent pDependant);

	default T ouput() {
		return get(null);
	}
}
