package be.functional.dataflow.core;

public interface IProperty<T> extends Value<T> {


	void set(T pValue);
}
